from model.db_model import DbModel


class User(DbModel):
   def __init__(self, **kwargs):
       self.idusuario: int = 0
       if(len(kwargs) > 0):
           for key in kwargs:
               setattr(self, key, kwargs[key])
       else:
           self.nomusuario: str = ''
           self.apeusuario: str = ''
           self.fechanac: str = ''
           self.email: str = ''
           self.telefono: int = 0
           self.password: str = ''
           self.alias: str = ''

   def set_attributes(self, *args, **kwargs):
       if len(args)>0:
           values = list(*args)
           self.idusuario = values[0]
           self.nomusuario = values[1]
           self.apeusuario = values[2]
           self.fechanac = values[3]
           self.email = values[4]
           self.telefono = values[5]
           self.password = values[6]
           self.alias = values[7]
       elif len(kwargs)>0:
           for key in kwargs:
               setattr(self, key, kwargs[key])

   def validate_password(self, password):
       return password == self.password

   def get_table_name(self) -> str:
       return 'usuario'

