from abc import ABC, abstractmethod


class DbModel(ABC):

    @abstractmethod
    def set_attributes(self, *args):
        ...
    @abstractmethod
    def get_table_name(self):
        ...

    def get_json(self):
        local_dir = dir(self)
        json = {}
        for attribute in local_dir:
            if not callable(getattr(self, attribute)) and not attribute.startswith('_'):
                json[attribute] = getattr(self, attribute)
        return json
