from model.db_model import DbModel


class Product(DbModel):
    def __init__(self, **kwargs):
        self.idproducto: int = 0
        self.image: str = ''
        if(len(kwargs) > 0):
            for key in kwargs:
                setattr(self, key, kwargs[key])
        else:
            self.nomprod: str = ''
            self.marcaprod: str = ''
            self.categoria: str = ''
            self.garant: int = 0
            self.cantprod: int = 0
            self.precio: float = 0
            self.especif: str = ''
            self.image: str = ''

    def set_attributes(self, *args):
        values = list(*args)
        self.idproducto = values[0]
        self.nomprod = values[1]
        self.marcaprod = values[2]
        self.categoria = values[3]
        self.garant = values[4]
        self.cantprod = values[5]
        self.precio = float(values[6])
        self.especif = values[7]
        self.image = values[8]



    def get_table_name(self) -> str:
        return 'productos'
