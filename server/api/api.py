import os
from flask import Flask, jsonify, request

from flask_cors import CORS, cross_origin

from db_connection import DbConnection
from db_queries import DbQueries
from model.product import Product
from model.User import User
from werkzeug.utils import secure_filename

UPLOAD_FOLDER = '../../UPLOADS'
ALLOWED_EXTENSIONS = ['pgn', 'jpg', 'jpeg']
app = Flask(__name__)
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-type'

connection = DbConnection()
dbProducts = DbQueries(connection.create_connection())


@app.route('/products/', methods=['GET'])
@cross_origin()
def get_products():
    product = Product()
    response = dbProducts.get(product)
    return jsonify(response)


@app.route('/products/<category>', methods=['GET'])
@cross_origin()
def get_products_by_category(category):
    product = Product()
    response = dbProducts.get_by_attribute(product, "categoria", category)
    return jsonify(response)


@app.route('/products/name/<name>', methods=['GET'])
@cross_origin()
def get_products_by_name(name):
    product = Product()
    response = dbProducts.get_by_attribute(product, 'nomprod', name)
    return jsonify(response)


@app.route('/productsOrdered/<name>', methods=['GET'])
@cross_origin()
def get_products_ordered(name):
    product = Product()
    response = dbProducts.get_products_ordered(product,"nomprod",name)
    print(response)
    if response == None:
        response = []
    return jsonify(response)


@app.route('/products/', methods=['POST'])
@cross_origin()
def post_productos():
    file = request.json['image']
    request.json['image'] = file['fileAsBase64']
    product = Product(**request.json)
    response = dbProducts.post(product)
    return jsonify(response)

@app.route('/usuarios/', methods=['GET'])
@cross_origin()
def get_users():
   usuario = User()
   response = dbProducts.get(usuario)
   return jsonify(response)

@app.route('/usuarios/', methods=['POST'])
@cross_origin()
def post_usuarios():
   user = User(**request.json)
   response = dbProducts.post(user)
   return jsonify(response)

@app.route('/usuarios/email/<email>', methods=['GET'])
@cross_origin()
def get_usuario_by_email(email):
    user = User()
    response = dbProducts.get_by_attribute(user, 'email', email)
    return jsonify(response)

@app.route('/usuarios/alias/<alias>', methods=['GET'])
@cross_origin()
def get_usuario_by_alias(alias):
    user = User()
    response = dbProducts.get_by_attribute(user, 'alias', alias)
    return jsonify(response)

@app.route('/login/', methods=['POST'])
@cross_origin()
def login_by_alias():
    response = []
    alias = request.json['alias']
    user = User()
    searched_user = dbProducts.get_by_attribute(user, 'alias', alias)
    if len(searched_user)>0:
        user.set_attributes(**searched_user[0])
        if user.validate_password(request.json['password']):
            response = searched_user
    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True)

