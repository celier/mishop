"""

"""
from typing import List

from model.db_model import DbModel


class DbQueries:

    def __init__(self, mydb):
        self.__dbConnection = mydb

    def get(self, db_model: DbModel) -> List:
        cursor = self.__dbConnection.cursor()
        cursor.execute(format("Select * from " + db_model.get_table_name()))
        items = cursor.fetchall()
        json_result = []
        for item in items:
            db_model.set_attributes(item)
            json_result.append(db_model.get_json())
        return json_result

    def post(self, db_model: DbModel):
        obj = db_model.get_json()
        values = str(tuple(obj.values()))
        columns = ','.join(list(obj))
        cursor = self.__dbConnection.cursor()
        cursor.execute("INSERT INTO {} ({}) VALUES {}".format(db_model.get_table_name(), columns, values))
        self.__dbConnection.commit()
        return db_model.get_json()

    def get_by_attribute(self, db_model: DbModel, column: str, value: str) -> list:
        cursor = self.__dbConnection.cursor()
        cursor.execute("Select * from {} where {}='{}'".format(db_model.get_table_name(), column, value))
        items = cursor.fetchall()
        jsonResult = []
        for item in items:
            db_model.set_attributes(item)
            jsonResult.append(db_model.get_json())
        return jsonResult

    def get_products_ordered(self, db_model: DbModel, column: str, value: str) -> list:
        cursor = self.__dbConnection.cursor()
        cursor.execute("Select * from {} where {}='{}'order by precio".format(db_model.get_table_name(), column, value))
        print(cursor)
        items = cursor.fetchall()
        jsonResult = []
        for item in items:
            db_model.set_attributes(item)
            jsonResult.append(db_model.get_json())
        return jsonResult



