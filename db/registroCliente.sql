CREATE TABLE `usuario` (
  `idusuario` int NOT NULL AUTO_INCREMENT,
  `nomusuario` varchar(50) NOT NULL,
  `apeusuario` varchar(45) NOT NULL,
  `fechanac` date DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `telefono` int DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `alias` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
)