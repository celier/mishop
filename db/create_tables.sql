CREATE TABLE `productos` (
  `idproducto` int NOT NULL AUTO_INCREMENT,
  `nomprod` varchar(100) NOT NULL,
  `marcaprod` varchar(30) NOT NULL,
  `categoria` varchar(20) NOT NULL,
  `garant` int DEFAULT NULL,
  `cantprod` int NOT NULL,
  `precio` decimal(19,4) NOT NULL,
  `especif` mediumtext,
  `image` MEDIUMTEXT NOT NULL ,
  PRIMARY KEY (`idproducto`)
);

CREATE TABLE `usuario` (
  `idusuario` int NOT NULL AUTO_INCREMENT,
  `nomusuario` varchar(50) NOT NULL,
  `apeusuario` varchar(45) NOT NULL,
  `fechanac` date DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `telefono` int DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `alias` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
)